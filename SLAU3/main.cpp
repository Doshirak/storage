#include <iostream>
#include <fstream>
#include <cstdlib>
#include <mpi.h>
#include "matrix.h"
#include "mpi_matrix.h"
#define BLOCK 0

using namespace std;

int main (int argc,char** argv)
{ 
    const int n = 1;
    int size,rank;
    MPI_Init(&argc,&argv);
    MPI_Comm_size(MPI_COMM_WORLD,&size);
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    ifstream* input1;
    ifstream* input2;
    ifstream* input3;
    ofstream* output;
    MpiMatrix A;
    MpiVector B;
    MpiVector X;

    if (rank == 0)
    {   
        if (argc < 3)
        {
            printf("need file names\n");
            MPI_Finalize();
            exit(0);
        }

        input1 = new ifstream;
        input2 = new ifstream;
        input3 = new ifstream;
        input1->open(argv[1]);
        input2->open(argv[2]);
        input3->open(argv[3]);

        if ((!*input1) || (!*input2) || (!*input3))
        {
            cout << "Can't open file" << endl;
            MPI_Finalize();
            exit(0);
        }
    }

    A << *input1;
    B << *input2;
    X << *input3;

    A.show();
    B.show();
    X.show();

    

    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Finalize();

	return 0;
}
