#ifndef BLOCKS_H_INCLUDED
#define BLOCKS_H_INCLUDED
#include <iostream>
#include <cstring>
#include <exception>

class Matrix
{
public:
	Matrix():data(NULL){};
	Matrix(int new_width,int new_height):
	width(new_width),height(new_height)
	{data = (double*) calloc (width * height,sizeof(double));
		memset(data,0,width * height * sizeof(double));};
	Matrix(const Matrix & matrix);
	Matrix(const Matrix & matrix,int x,int y,int new_width,int new_height);
	void set(const Matrix & matrix,int x,int y,int new_width,int new_height);
	void put(const Matrix & matrix,int x,int y);
	~Matrix(){delete data;};
	Matrix& operator*(double);
	Matrix& operator=(const Matrix & a);
	bool operator==(const Matrix & a);
	bool operator!=(const Matrix & a);
	std::istream& operator<<(std::istream& a);
	std::ostream& operator>>(std::ostream& a);
	double & operator[](int x) {return data[x];}; 
	friend Matrix operator*(const Matrix& left,const Matrix& right);
	const double & operator[](int x) const {return data[x];}; 
	const int & get_width() const {return width;};
	const int & get_height() const {return height;};
	double * get_data() {return data;};
	int get_size(){return width * height;};
	void show() const;
protected:
	int height;
	int width;
	double* data;
};

class Vector: public Matrix
{
public:
	Vector(){Matrix();};
	Vector(int new_width,int new_height):
	Matrix(new_width,new_height)
	{
		if ((Matrix::width != 1) && (Matrix::height != 1))
		{
			throw;
		}
	}
	Vector& operator=(const Vector & a)
	{
		Matrix::operator=(a);
		return *this;
	}
	friend Vector operator*(const Matrix& left,const Vector& right);
	friend Vector operator-(const Vector& left,const Vector& right);
	friend Vector operator+(const Vector& left,const Vector& right);
};

double scalar(const Vector& left,const Vector& right);

#endif