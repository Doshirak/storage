#include <iostream>
#include <cstdlib>
#include <cstdio>
#include "matrix.h"
using namespace std;

Matrix::Matrix(const Matrix & matrix)
{
	width = matrix.get_width();
	height = matrix.get_height();
	data = (double*) calloc (width * height,sizeof(double));
	for (int i = 0;i < height;++i)
	{
		for (int j = 0;j < width;++j)
		{
			data[i * width + j] = matrix[i * width + j];
		}
	}
}

istream& Matrix::operator<<(istream& a)
{
	delete[] data;
	a >> width >> height;
	data = (double*) calloc (width * height,sizeof(double));
	for (int i = 0;i < height;++i)
	{
		for (int j = 0;j < width;++j)
		{
			a >> data[i * width + j];
		}
	}
}

ostream& Matrix::operator>>(ostream& a)
{
	for (int i = 0;i < height;++i)
	{
		for (int j = 0;j < width;++j)
		{
			a << data[i * width + j] << " ";
		}
		a << endl;
	}
}

Matrix operator*(const Matrix& left, const Matrix& right)
{
	//cout << left.get_height() << " " << right.get_width() << endl;
	Matrix res(right.get_width(),left.get_height());
	left.show();
	right.show();
	if (left.get_width() != right.get_height())
	{
		printf("Wrong sizes: %d and %d\n",left.get_height(),right.get_width());
		throw;
	}
	for (int i = 0;i < left.get_height();++i)
	{
		for(int j = 0;j < right.get_width();++j)
		{
			double summ = 0;
			for(int k = 0;k < left.get_width();++k)
			{
				summ += left[i * left.get_width() + k] 
				* right[k * right.get_width() + j];
			}
			res[i * res.get_width() + j] = summ;
		}
	}
	return res;
}

Matrix::Matrix(const Matrix & matrix,int x,int y,int new_width,int new_height):
width(new_width),height(new_height)
{
	data = (double*) calloc (width * height,sizeof(double));
	for (int i = 0;i < height;++i)
	{
		for (int j = 0;j < width;++j)
		{
			data[(y + i) * width + j] = matrix[(y + i) * matrix.get_width() + j + x];
		}
	}
}

void Matrix::set(const Matrix & matrix,int x,int y,int new_width,int new_height)
{
	width = new_width;
	height = new_height;
	delete[] data;
	data = (double*) calloc (width * height,sizeof(double));
	for (int i = 0;i < height;++i)
	{
		for (int j = 0;j < width;++j)
		{
			data[i * width + j] = matrix[(y + i) * matrix.get_width() + j + x];
		}
	}
}

void Matrix::put(const Matrix & matrix,int x,int y)
{
	for (int i = 0;i < matrix.get_height();++i)
	{
		for (int j = 0;j < matrix.get_width();++j)
		{
			data[(i + y)*width + j + x] = matrix[i * matrix.get_width() + j];
		}
	}
}

Matrix& Matrix::operator*(double a)
{
	for (int i = 0;i < height;++i)
	{
		for (int j = 0;j < width;++j)
		{
			data[i * width + j] *= a;
		}
	}
	return *this;
}

Matrix& Matrix::operator=(const Matrix & matrix)
{
	if (this == &matrix)
	{
		return *this;
	}
	delete[] data;
	width = matrix.get_width();
	height = matrix.get_height();
	data = (double*) calloc (width * height,sizeof(double));
	data = (double*) calloc (width * height,sizeof(double));
	for (int i = 0;i < height;++i)
	{
		for (int j = 0;j < width;++j)
		{
			data[i * width + j] = matrix[i * width + j];
		}
	}
	return *this;
}

bool Matrix::operator==(const Matrix & a)
{
	if (this == &a)
	{
		return true;
	}
	if ((height != a.get_height()) || (width != a.get_width()))
	{
		return false;
	}
	for (int i = 0;i < height;++i)
	{
		for (int j = 0;j < width;++j)
		{
			if (data[i*width + j] != a[i*width + j])
			{
				return false;
			}
		}
	}
}

bool Matrix::operator!=(const Matrix & a)
{
	return !(*this == a);
}

void Matrix::show() const
{
	for (int i = 0;i < height;++i)
	{
		for (int j = 0;j < width;++j)
		{
			printf("%.0f ",data[i*width + j]);
		}
		printf("\n");
	}
	printf("\n");
}

Vector operator*(const Matrix& left,const Vector& right)
{
	if (left.get_width() != right.get_height())
	{
		printf("Wrong sizes: %d and %d\n",left.get_width(),right.get_height());
		throw;
	}
	Vector res(right.get_width(),left.get_height());
	for (int i = 0;i < left.get_height();++i)
	{
		for(int j = 0;j < right.get_width();++j)
		{
			double summ = 0;
			for(int k = 0;k < left.get_width();++k)
			{
				summ += left[i * left.get_width() + k] 
				* right[k * right.get_width() + j];
			}
			res[i * res.get_width() + j] = summ;
		}
	}
	return res;
}

Vector operator-(const Vector& left,const Vector& right)
{
	if (left.get_height() != right.get_height())
	{
		printf("Wrong sizes: %d and %d\n",left.get_height(),right.get_height());
		throw;
	}
	Vector res(right.get_width(),left.get_height());
	for (int i = 0;i < left.get_height();++i)
	{
		for(int j = 0;j < right.get_width();++j)
		{
			res[i * res.get_width() + j] = left[i * left.get_width() + j] - right[i * right.get_width() + j];
		}
	}
	return res;
}

Vector operator+(const Vector& left,const Vector& right)
{
	if (left.get_height() != right.get_height())
	{
		printf("Wrong sizes: %d and %d\n",left.get_height(),right.get_height());
		throw;
	}
	Vector res(right.get_width(),left.get_height());
	for (int i = 0;i < left.get_height();++i)
	{
		for(int j = 0;j < right.get_width();++j)
		{
			res[i * res.get_width() + j] = left[i * left.get_width() + j] + right[i * right.get_width() + j];
		}
	}
	return res;
}

double scalar(const Vector& left,const Vector& right)
{
	if (left.get_height() != right.get_height())
	{
		printf("Wrong sizes: %d and %d\n",left.get_height(),right.get_height());
		throw;
	}
	double sum = 0;
	for (int i = 0;i < left.get_height();++i)
	{
		for(int j = 0;j < right.get_width();++j)
		{
			sum += left[i * left.get_width() + j] * right[i * right.get_width() + j];
		}
	}
	return sum;
}