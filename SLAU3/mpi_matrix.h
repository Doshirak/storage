#ifndef MPI_MATRIX_INCLUDED
#define MPI_MATRIX_INCLUDED
#include "matrix.h"

/*
Matrices a and b exists only in process 0
*/

class MpiMatrix 
{
public:
	MpiMatrix();
	MpiMatrix(int new_width,int new_height);
	~MpiMatrix();
	std::istream& operator<<(std::istream& a);
	std::ostream& operator>>(std::ostream& a);
	MpiMatrix& operator=(const Matrix * a);
	MpiMatrix& operator=(const MpiMatrix & a);
	friend MpiMatrix operator*(MpiMatrix& left,MpiMatrix& right);
	const Matrix* getMatrix() const;
	Matrix* getMatrix();
	void show();
protected:
	int size;
	int rank;
	Matrix * matrix;
};
	
class MpiVector : public MpiMatrix 
{
public:
	MpiVector();
	MpiVector(int new_width,int new_height);
	~MpiVector();
	MpiVector& operator=(const Vector * a);
	MpiVector& operator=(const MpiVector & a);
	MpiVector& operator*(double);
	friend MpiVector operator*(MpiMatrix& left,MpiVector& right);
	friend MpiVector operator-(MpiVector& left,MpiVector& right);
	friend MpiVector operator+(MpiVector& left,MpiVector& right);
	friend double scalar(MpiVector& left,MpiVector& right);
};

Matrix * mpi_mul(Matrix * a, Matrix * b);
Vector * mpi_mul(Matrix * a, Vector * b);

#endif