#include <cstdlib>
#include <mpi.h>
#include "matrix.h"
#include "mpi_matrix.h"
using namespace std;

MpiMatrix::MpiMatrix()
{
    MPI_Comm_size(MPI_COMM_WORLD,&size);
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);

    if (rank == 0)
    {
        matrix = new Matrix();
    }
};

MpiMatrix::MpiMatrix(int new_width,int new_height)
{
    MPI_Comm_size(MPI_COMM_WORLD,&size);
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);

    if (rank == 0)
    {
        matrix = new Matrix(new_width,new_height);
    }
};

MpiMatrix::~MpiMatrix()
{
    if (rank == 0)
    {
        delete matrix;
    }
};

std::istream& MpiMatrix::operator<<(std::istream& a)
{
    MPI_Comm_size(MPI_COMM_WORLD,&size);
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);

    if (rank == 0)
    {
        *matrix << a;
    }
}

std::ostream& MpiMatrix::operator>>(std::ostream& a)
{
    MPI_Comm_size(MPI_COMM_WORLD,&size);
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);

    if (rank == 0)
    {
        *matrix >> a;
    }
}

MpiMatrix& MpiMatrix::operator=(const Matrix * a)
{
    if (rank == 0)
    {
        if (*matrix != *a)
        {
            *matrix  = *a;
        }
    }
    return *this;
}

MpiMatrix& MpiMatrix::operator=(const MpiMatrix & a)
{
    if (this == &a)
    {
        return *this;
    }
    if (rank == 0)
    {
        if (*matrix == *(a.getMatrix()))
        {
            return *this;
        }
        *matrix = *(a.getMatrix());
    }
    return *this;
}

MpiMatrix operator*(MpiMatrix& left,MpiMatrix& right)
{
    MpiMatrix result;
    result = mpi_mul(left.getMatrix(),right.getMatrix());
    return result;
}

Matrix* MpiMatrix::getMatrix()
{
    return matrix;
}

const Matrix* MpiMatrix::getMatrix() const
{
    return matrix;
}

void MpiMatrix::show()
{
    if (rank == 0)
    {
        matrix->show();
    }
}

MpiVector::MpiVector()
{}

MpiVector::MpiVector(int new_width,int new_height)
:MpiMatrix(new_width,new_height)
{
    if (rank == 0)
    {
        if ((getMatrix()->get_width() != 1) && (getMatrix()->get_height() != 1))
        {
            throw;
        }
    }
}

MpiVector::~MpiVector(){};

MpiVector& MpiVector::operator= (const Vector* a)
{
    if (rank == 0)
    {
        if (*matrix != *a)
        {
            *matrix  = *a;
        }
    }
    return *this;
}

MpiVector& MpiVector::operator= (const MpiVector& a)
{
    if (this == &a)
    {
        return *this;
    }
    if (rank == 0)
    {
        if (*matrix == *(a.getMatrix()))
        {
            return *this;
        }
        *matrix = *(a.getMatrix());
    }
    return *this;   
}

MpiVector& MpiVector::operator*(double a)
{
    if (rank == 0)
    {
        *matrix = *matrix * a;
    } 
    return *this;
}

MpiVector operator*(MpiMatrix& left,MpiVector& right)
{
    MpiVector result;
    result = mpi_mul(left.getMatrix(),(Vector*)right.getMatrix());
    return result; 
}

MpiVector operator-(MpiVector& left,MpiVector& right)
{
    int size,rank;
    MPI_Comm_size(MPI_COMM_WORLD,&size);
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    MpiVector result;
    Matrix * matrix = result.getMatrix();
    if (rank == 0)
    {
        *matrix = *((Vector*)left.getMatrix()) - *((Vector*)right.getMatrix());    
    }
    return result;    
}

MpiVector operator+(MpiVector& left,MpiVector& right)
{
    int size,rank;
    MPI_Comm_size(MPI_COMM_WORLD,&size);
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    MpiVector result;
    Matrix * matrix = result.getMatrix();
    if (rank == 0)
    {
        *matrix = *((Vector*)left.getMatrix()) + *((Vector*)right.getMatrix());    
    }
    return result;    
}

double scalar(MpiVector& left,MpiVector& right)
{
    int size,rank;
    MPI_Comm_size(MPI_COMM_WORLD,&size);
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    double result;
    if (rank == 0)
    {
        result = scalar(*((Vector*)left.getMatrix()),*((Vector*)right.getMatrix()));    
    }
    MPI_Bcast(&result,1,MPI_INT,0,MPI_COMM_WORLD);
    return result;   
}

Matrix * mpi_mul(Matrix * matrix_a, Matrix * matrix_b)
{
    int size,rank;
	int width_a,height_a;
    int width_b,height_b;
    int block_size_a;
    int block_size_b;
    Matrix * matrix_c = NULL;

    MPI_Comm_size(MPI_COMM_WORLD,&size);
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);

    if (rank == 0)
    {
    	width_a = matrix_a->get_width();
        height_a = matrix_a->get_height();
        width_b = matrix_b->get_width();
        height_b = matrix_b->get_height();
        matrix_c = new Matrix(width_b,height_a);
    } 



    MPI_Bcast(&width_a,1,MPI_INT,0,MPI_COMM_WORLD);
    MPI_Bcast(&height_a,1,MPI_INT,0,MPI_COMM_WORLD);
    MPI_Bcast(&width_b,1,MPI_INT,0,MPI_COMM_WORLD);
    MPI_Bcast(&height_b,1,MPI_INT,0,MPI_COMM_WORLD);

    block_size_a = height_a / size + (height_a % size) * (rank == size - 1); 
    block_size_b = width_b / size + (width_b % size) * (rank == size - 1); 

    double* ptr_a = NULL;
    double* ptr_b = NULL;
    double* ptr_c = NULL;
    if (rank == 0)
    {
        ptr_a = matrix_a->get_data();
        ptr_b = matrix_b->get_data();
        ptr_c = matrix_c->get_data();
    }

    Matrix buffer_a (width_a,block_size_a);
    Matrix buffer_b (block_size_b,width_b);

    int counts_a[size];
    int displs_a[size];
    int counts_b[size];
    int displs_b[size];
    for (int i = 0;i < size;++i)
    {
        counts_a[i] = (height_a / size + (height_a % size) * (i == size - 1)) * width_a;
        displs_a[i] = (height_a / size) * width_a * i;
        counts_b[i] = width_b / size + (width_b % size) * (rank == size - 1);
        displs_b[i] = (width_b / size) * i; 
    }

    MPI_Scatterv(ptr_a,counts_a,displs_a,MPI_DOUBLE,buffer_a.get_data(),
       block_size_a*width_a,MPI_DOUBLE,0,MPI_COMM_WORLD);

    for (int i = 0;i < height_b;++i)
    {
        MPI_Scatterv(&(ptr_b[i * width_b]),counts_b,displs_b,MPI_DOUBLE,&(buffer_b.get_data()[i * block_size_b]),
        block_size_b,MPI_DOUBLE,0,MPI_COMM_WORLD);
    }

    for(int k = 0;k < size;++k)
    {
        Matrix buffer_c = buffer_a * buffer_b;

        MPI_Datatype BLOCK_RAW; 
        MPI_Type_vector( buffer_c.get_height(), buffer_c.get_width(), width_b, MPI_DOUBLE, &BLOCK_RAW); 
        MPI_Type_commit( &BLOCK_RAW );
        MPI_Datatype BLOCKS; 
        int block_len[2] = {1,1};
        MPI_Aint displs1[2] = {0,width_b};
        MPI_Datatype types[2] = {BLOCK_RAW,MPI_UB};
        MPI_Type_create_struct(2,block_len,displs1,types,&BLOCKS);
        MPI_Type_commit( &BLOCKS);

        int displs2[size];
        int recvcount[size];
        for (int i = 0;i < size;++i)
        {
            displs2[i] = i * (2 * (width_b + 1)) - width_b/(width_b/(2*size))*(i >= size - k);
            recvcount[i] = (height_a / size + (height_a % size) * (i == size - 1)) * width_b / size + (width_b % size) * (rank == size - 1); 
        }
        
        MPI_Gatherv(buffer_c.get_data(),buffer_c.get_size(),MPI_DOUBLE
                ,&ptr_c[k*(width_b/size)],recvcount,displs2,BLOCKS,0,MPI_COMM_WORLD);


        MPI_Sendrecv (buffer_b.get_data(),buffer_b.get_size(),MPI_DOUBLE,(rank+size-1)%size,1,
                    buffer_b.get_data(),buffer_b.get_size(),MPI_DOUBLE,(rank+1)%size,1,
                    MPI_COMM_WORLD,MPI_STATUS_IGNORE);        
    }
    
    
    MPI_Barrier(MPI_COMM_WORLD);

    return matrix_c;
}

Vector * mpi_mul(Matrix * matrix_a, Vector * vector_b)
{
    int size,rank;
    int width_a,height_a;
    int width_b,height_b;
    int block_size_a;
    int block_size_b;
    Vector * vector_c = NULL;

    MPI_Comm_size(MPI_COMM_WORLD,&size);
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);

    if (rank == 0)
    {
        width_a = matrix_a->get_width();
        height_a = matrix_a->get_height();
        width_b = vector_b->get_width();
        height_b = vector_b->get_height();
        vector_c = new Vector(width_b,height_a);
    }

    MPI_Bcast(&width_a,1,MPI_INT,0,MPI_COMM_WORLD);
    MPI_Bcast(&height_a,1,MPI_INT,0,MPI_COMM_WORLD);
    MPI_Bcast(&width_b,1,MPI_INT,0,MPI_COMM_WORLD);
    MPI_Bcast(&height_b,1,MPI_INT,0,MPI_COMM_WORLD);

    if (rank != 0)
    {
        vector_b = new Vector(width_b,height_b);
    }

    MPI_Bcast(vector_b->get_data(),vector_b->get_size(),MPI_DOUBLE,0,MPI_COMM_WORLD);

    block_size_a = height_a / size + (height_a % size) * (rank == size - 1); 
    block_size_b = width_b / size + (width_b % size) * (rank == size - 1); 

    double* ptr_a = NULL;
    double* ptr_b = NULL;
    double* ptr_c = NULL;
    if (rank == 0)
    {
        ptr_a = matrix_a->get_data();
        ptr_b = vector_b->get_data();
        ptr_c = vector_c->get_data();
    }

    Matrix buffer_a (width_a,block_size_a);
    Matrix buffer_b (block_size_b,width_b);

    int counts_a[size];
    int displs_a[size];
    int counts_b[size];
    int displs_b[size];
    for (int i = 0;i < size;++i)
    {
        counts_a[i] = (height_a / size + (height_a % size) * (i == size - 1)) * width_a;
        displs_a[i] = (height_a / size) * width_a * i;
        counts_b[i] = width_b / size + (width_b % size) * (rank == size - 1);
        displs_b[i] = (width_b / size) * i; 
    }

    MPI_Scatterv(ptr_a,counts_a,displs_a,MPI_DOUBLE,buffer_a.get_data(),
      block_size_a*width_a,MPI_DOUBLE,0,MPI_COMM_WORLD);

    for(int k = 0;k < size;++k)
    {
        //cout << buffer_a.get_width() << " " << vector_b->get_height() << endl;
        Vector buffer_c = buffer_a * (*vector_b);
        
        MPI_Gather(buffer_c.get_data(),buffer_c.get_size(),MPI_DOUBLE
                ,ptr_c,buffer_c.get_size(),MPI_DOUBLE,0,MPI_COMM_WORLD);
     }
    
    
    MPI_Barrier(MPI_COMM_WORLD);

    return vector_c;
}
