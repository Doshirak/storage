#include <mpi.h>
#include <iostream>
#include <cstdlib>
#include <fstream>
#include <cstring>
#define DEFAULT 0
#define MAX 100

using namespace std;

int main (int argc, char* argv[])
{
	// Init
	int rank,size;
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD,&rank);
	MPI_Comm_size(MPI_COMM_WORLD,&size);
	double* A;
	int widthA, heightA;
	double* B;
	int widthB, heightB;
	double* C;
	
	// Debug data
	int debug = 0;
	if (argc > 1)
		debug = atoi(argv[1]);
	char fileName[MAX];
	ofstream file;
	if (rank)
	{
		sprintf(fileName,"%dDump",rank);
		file.open(fileName);
	}
	ostream & output = (!rank) ? cout : file;

	// Read files
	if(!rank)
	{
		ifstream inputA;
		ifstream inputB;
		inputA.open("A");
		inputB.open("B");

		inputA >> widthA >> heightA;
		inputB >> widthB >> heightB;

		if ((widthA != heightB) || (widthB != heightA))
		{
			cout << "wrong sizes" << endl << "exiting..." << endl;
			exit(0);
		}

		A = new double[widthA * heightA];
		if (debug == 1)
			cout << "A " << heightA << " " << widthA << endl; 
		for (int i = 0;i < heightA;++i)
		{
			for(int j = 0;j < widthA;++j)
			{
				inputA >> A[widthA * i + j];
				if (debug == 1)
					cout << A[widthA * i + j] << " ";
			}
			if (debug == 1)
				cout << endl;
		}

		B = new double[widthB * heightB]; 
		if (debug == 1)
			cout << "B " << heightB << " " << widthB << endl;
		for (int i = 0;i < heightB;++i)
		{
			for(int j = 0;j < widthB;++j)
			{
				inputB >> B[widthB * i + j];
				if (debug == 1)
					cout << B[widthB * i + j] << " ";
			}
			if (debug == 1)
				cout << endl;
		}

		C = new double[heightA * widthB];
		for (int i = 0;i < heightA;++i)
			for (int j = 0;j < widthB;++j)
				C[i * widthB + j] = 0;

		inputA.close();
		inputB.close();
	}

	MPI_Bcast(&widthA,1,MPI_INT,0,MPI_COMM_WORLD);
	MPI_Bcast(&heightA,1,MPI_INT,0,MPI_COMM_WORLD);
	MPI_Bcast(&widthB,1,MPI_INT,0,MPI_COMM_WORLD);
	MPI_Bcast(&heightB,1,MPI_INT,0,MPI_COMM_WORLD);

	// Count block size for processes  
	int blockHeightA = heightA / size + heightA % size;
	int blockWidthB = blockHeightA;

	int last = size - 1;
	int countsA[size];
	int displsA[size];
	int countsB[size];
	int displsB[size];

	// Send data to blocks
	for (int i = 0;i < size;++i)
	{
		countsA[i] = (heightA / size + heightA % size * (i == last)) * widthA;
		displsA[i] = heightA / size * i * widthA;
		if (countsA[i] == 0)
		{
			output << "too many process" << endl << "exiting..." << endl;
			exit(0);
		}
		if (debug == 2)
			output << "A i " << i << " displs " << displsA[i] << " counts " << countsA[i] << endl;
		countsB[i] = widthB / size + widthB % size * (i == last);
		displsB[i] = widthB / size * i;
		if (debug == 2)
			output << "B i " << i << " displs " << displsB[i] << " counts " << countsB[i] << endl;
	}

	double* buffA = new double[blockHeightA * widthA];
	memset(buffA,0,blockHeightA * widthA);
	double* buffB = new double[blockWidthB * heightB];
	memset(buffB,0,blockWidthB * heightB);

	// Send rows of matrix A
	MPI_Scatterv(A, countsA, displsA, MPI_DOUBLE, buffA, blockHeightA * widthA, MPI_DOUBLE, 0, MPI_COMM_WORLD);
	if (debug == 3)
	{
		output << "A" << endl;
		for (int i = 0;i < blockHeightA;++i)
		{
			for (int j = 0;j < widthA;++j)
				output << buffA[i * widthA + j] << " ";
			output << endl;
		}
	}

	// Send columns of matrix B
	for (int i = 0;i < heightB;++i)
    {
        MPI_Scatterv(B, countsB, displsB, MPI_DOUBLE, buffB + i * blockWidthB,
        blockWidthB,MPI_DOUBLE,0,MPI_COMM_WORLD);
    }
	if (debug == 3)
	{
		output << "B" << endl;
		for (int i = 0;i < heightB;++i)
		{
			for (int j = 0;j < blockWidthB;++j)
				output << buffB[i * blockWidthB + j] << " ";
			output << endl;
		}
	}    


	double* buffC = new double[blockHeightA * blockWidthB];

	int currWidth = heightA / size + heightA % size * (rank == last); 
	int currHeight = widthB / size + widthB % size * (rank == last);
	int prevHeight = 0;
	int prevWidth = 0;

	// Start calculation
	for (int k = 0;k < size;++k)
	{
		if (debug == 4)
			output << "k " << k << " currWidth " << currWidth << " currHeight " << currHeight << endl;  
		
		// Multiple block
		for (int i = 0;i < currHeight;++i)
		{
			for (int j = 0;j < currWidth;++j)
			{
				double sum  = 0;
				for (int s = 0;s < widthA;++s)
				{
					sum += buffA[i * widthA + s] * buffB[s * blockWidthB + j];
				}
				buffC[i * currWidth + j] = sum;
				if (debug == 4)
					output << buffC[i * currWidth + j] << " ";
			}
			if (debug == 4)
				output << endl;
		}

		// If zero procces - write to final matrix own block and receive from others
		if (!rank)
		{
			for (int i = 0;i < currHeight;++i)
			{
				for (int j = 0;j < currWidth;++j)
				{
					C[(i + rank * blockHeightA)* widthB + j + k * prevWidth] = buffC[i * currWidth + j];
				}
			}
			

			// Receive strings sof matrix from each process
			int tempHeight = currHeight;
			int tempWidth = currWidth;
			for (int r = 1;r < size;++r)
			{
				int prevTempHeight = tempHeight;
				int prevTempWidth = tempWidth;
				MPI_Recv(&tempHeight,1,MPI_INT,r,DEFAULT,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
				MPI_Recv(&tempWidth,1,MPI_INT,r,DEFAULT,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
				if (debug == 5)
					cout << "r " << r << " height " << tempHeight << " width " << tempWidth << endl;
				for (int i = 0;i < tempHeight;++i)
				{
					int x = (r + k) % size;
					int offset = (i + r * prevTempHeight)* widthB + x * prevTempWidth;
					if (debug == 5)
						cout << "y " << (i + r * prevTempHeight) << " x " << r * prevTempWidth << endl;
					MPI_Recv(C + offset,tempWidth,MPI_DOUBLE,r,DEFAULT,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
				}
			}

			if (debug == 5)
				for (int i = 0;i < heightA;++i)
				{
					for (int j = 0;j < widthB;++j)
						output << C[i * widthB + j] << " ";
					output << endl;
				}
		}
		// If non zero process, send rows 
		else
		{
			MPI_Send(&currHeight,1,MPI_INT,0,DEFAULT,MPI_COMM_WORLD);
			MPI_Send(&currWidth,1,MPI_INT,0,DEFAULT,MPI_COMM_WORLD);
			for (int i = 0;i < currHeight;++i)
			{
				MPI_Send(buffC + i * currWidth,currWidth,MPI_DOUBLE,0,DEFAULT,MPI_COMM_WORLD);
			}
		}

		// If we have few processes swap data in blocks 
		if (size < 2)
			continue;
		int right = (rank + 1) % size;
		int left = (rank - 1) < 0 ? last : rank - 1;
		if (debug == 6)
			output << "rank " << rank << " right " << right << " left " << left << endl;
			
		prevWidth = currWidth;
		prevHeight = currHeight;	
		MPI_Sendrecv(&currWidth, 1, MPI_DOUBLE, right, DEFAULT, &currWidth, 1, MPI_DOUBLE, left, DEFAULT, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		MPI_Sendrecv(&currHeight, 1, MPI_DOUBLE, right, DEFAULT, &currHeight, 1, MPI_DOUBLE, left, DEFAULT, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		MPI_Sendrecv(buffB, heightB * blockWidthB, MPI_DOUBLE, right, DEFAULT, buffB, heightB * blockWidthB, MPI_DOUBLE, left, DEFAULT, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		if (debug == 6)
		{
			for (int i = 0;i < heightB;++i)
			{
				for (int j = 0;j < currWidth;++j)
					output << buffB[i * blockWidthB + j] << " ";
				output << endl;
			}
		}
	}

	// Save matrix C to file
	if (!rank)
	{
		ofstream fileC;
		fileC.open("C");

		for (int i = 0;i < heightA;++i)
		{
			for (int j = 0;j < widthB;++j)
			{
				fileC << C[i * widthB + i] << " ";
			}
			fileC << endl;
		}

		fileC.close();


		// Cleaning up 
		delete[] A;
		delete[] B;
		delete[] C;
	}
	delete[] buffA;
	delete[] buffB;
	delete[] buffC;	
	file.close();
	MPI_Finalize();
	return 0;
}